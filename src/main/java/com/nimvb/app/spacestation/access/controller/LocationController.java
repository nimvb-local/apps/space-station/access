package com.nimvb.app.spacestation.access.controller;

import com.nimvb.app.spacestation.access.model.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/stations/locations")
@RequiredArgsConstructor
public class LocationController {
    private final ReactiveMongoTemplate mongoTemplate;
    @GetMapping(value = "/",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Message> messages(){
        return mongoTemplate.tail(new Query(),Message.class);
    }
}
