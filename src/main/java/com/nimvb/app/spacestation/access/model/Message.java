package com.nimvb.app.spacestation.access.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "location")
@Data
public class Message {
    @Id
    @JsonProperty("_id")
    @Field("_id")
    private ObjectId objectId;
    @JsonProperty("id")
    private String id;
    @JsonProperty("response_timestamp")
    private long timestamp;
    @JsonProperty("data")
    private Payload payload;
}
