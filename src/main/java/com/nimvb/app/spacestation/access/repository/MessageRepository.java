package com.nimvb.app.spacestation.access.repository;

import com.nimvb.app.spacestation.access.model.Message;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends ReactiveMongoRepository<Message, ObjectId> {
}
