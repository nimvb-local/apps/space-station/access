package com.nimvb.app.spacestation.access.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
@CrossOrigin(origins = "*",methods = {RequestMethod.GET,RequestMethod.OPTIONS,RequestMethod.HEAD})
public class IndexController {

    @GetMapping
    public String index(){
        return "index";
    }
}
